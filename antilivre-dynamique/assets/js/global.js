// Lazy

const optionsLazy = {
  // Your custom settings go here
  elements_selector: '.lazyload',
  // Native lazy loading
  // use_native: true

}
const lazyLoadInstance = new LazyLoad(optionsLazy);

//// Menu
const menuBtn = document.querySelector(".button--menu");
const menu = document.querySelector(".menu");
let menuOpen = false;

const fabrique = document.querySelector('.button--shuffle');
const clearBtn = document.querySelector('.button--clear');
const paysageBtn = document.querySelector('.button--paysage');
const options = document.querySelector('.options');
const contenu = document.querySelector('.contenu');
const contenuDessin = document.querySelector('.contenu__image--visible');
const dessins = Array.from(document.querySelectorAll('.contenu__image img'));


const loadBtn = document.querySelector(".btn--load");

loadBtn.addEventListener("click", (e) => {
  e.preventDefault();
  loadBtn.blur();
  document.querySelector('.loading').classList.add("loading--done");
  clear();
});


menuBtn.addEventListener("click", (e) => {
  e.preventDefault();
  menuBtn.blur();
  menuOpen = !menuOpen;
  if (menuOpen) {
    // if (!isTouchDevice()) controls.enabled = false;
  } else {
    // if (!isTouchDevice()) controls.enabled = true;
  }
  menu.classList.toggle("show--menu");
});


//
// Scripts hack
//
// Correct 100 Vh - Source : https://codepen.io/team/css-tricks/pen/WKdJaB
// First we get the viewport height and we multiple it by 1% to get a value for a vh unit
let vh = window.innerHeight * 0.01;
// Then we set the value in the --vh custom property to the root of the document
document.documentElement.style.setProperty("--vh", `${vh}px`);

// We listen to the resize event
window.addEventListener("resize", () => {
  // We execute the same script as before
  let vh = window.innerHeight * 0.01;
  document.documentElement.style.setProperty("--vh", `${vh}px`);
});

// Prevent bounce effect iOS
// Source https://gist.github.com/swannknani/eca799795860cff222f70b8675f8c8d8
var content = document.querySelector('.contenu');
content.addEventListener('touchstart', function (event) {
    this.allowUp = (this.scrollTop > 0);
    this.allowDown = (this.scrollTop < this.scrollHeight - this.clientHeight);
    this.slideBeginY = event.pageY;
});

content.addEventListener('touchmove', function (event) {
    var up = (event.pageY > this.slideBeginY);
    var down = (event.pageY < this.slideBeginY);
    this.slideBeginY = event.pageY;
    if ((up && this.allowUp) || (down && this.allowDown)) {
        event.stopPropagation();
    }
    else {
        event.preventDefault();
    }
});

// Shuffle array
function shuffleArray(array) {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
  return array;
}

function Affiche(init) {
  if (init) {
    contenuDessin.innerHTML = "";
    shuffleArray(dessins);
    let dessin = dessins[0];
    let dessinNext = dessins[1];
    contenuDessin.appendChild(dessin);
    contenuDessin.appendChild(dessinNext);
  } else {
    shuffleArray(dessins);
    let dessin = dessins[0];
    let firstImg = contenuDessin.querySelector('img:first-child');
    if (dessin === firstImg) {
      dessin = dessins[1];
    }
    contenuDessin.appendChild(dessin);
    contenuDessin.removeChild(firstImg);
    // contenuDessin.appendChild(dessinNext);
  }
}

// Custom cursor
const cursor = document.querySelector('.cursor--main');
const cursorBg = document.querySelector('.cursor--bg');
const links = document.querySelectorAll('a, input, .button');

let cursorX = -100;
let cursorY = -100;
let cursorBgX = -100;
let cursorBgY = -100;
let pageLoad = true;

const cursorMove = () => {
  document.addEventListener('mousemove', (e) => {
    cursorX = e.clientX;
    cursorY = e.clientY;
    if (pageLoad) {
      cursorBgX = e.clientX;
      cursorBgY = e.clientY;
      pageLoad = !pageLoad;
    }
  });

  // Lerp (linear interpolation)
  const lerp = (start, finish, speed) => {
    return (1 - speed) * start + speed * finish;
  };

  const rendering = () => {
    cursorBgX = lerp(cursorBgX, cursorX, 0.1);
    cursorBgY = lerp(cursorBgY, cursorY, 0.1);
    cursor.style.left = `${cursorX}px`;
    cursor.style.top = `${cursorY}px`;
    cursorBg.style.left = `${cursorBgX}px`;
    cursorBg.style.top = `${cursorBgY}px`;
    requestAnimationFrame(rendering);
  };
  requestAnimationFrame(rendering);
}

cursorMove();


// Interaction with the cursor
 links.forEach((e) => {
   e.addEventListener('mouseenter', () => {
     cursor.classList.add('cursor--interact');
   });
   e.addEventListener('mouseleave', () => {
     cursor.classList.remove('cursor--interact');
   });
 });

 document.addEventListener('click', () => {
   cursor.classList.add('cursor--click');

   setTimeout(() => {
     cursor.classList.remove('cursor--click');
   }, 250);
 });

fabrique.addEventListener('click', (e) => {
  e.preventDefault();
  if (menuOpen) {
    menuOpen = !menuOpen;
    menu.classList.remove("show--menu");
  }
  Affiche();
  fabrique.blur();
});

let downloadImage = document.querySelector('.button--img');
downloadImage.addEventListener('click', (e) => {
  e.preventDefault();
  downloadImage.blur();
  if (menuOpen) {
    menuOpen = !menuOpen;
    menu.classList.remove("show--menu");
  }
  options.classList.add("hide");
  document.documentElement.classList.add("hide-scrollbar");

  html2canvas(document.querySelector('.contenu'),{
    allowTaint: false,
    logging: false,
    backgroundColor: "#ffffff",
    letterRendering: true,
    scale: 2
  }).then(function(canvas) {
    // const link = document.createElement('a');
    // document.body.appendChild(link);
    // link.download = 'dio.jpg';
    // link.href = canvas.toDataURL("image/jpeg").replace("image/jpeg", "image/octet-stream");
    // link.target = '_blank';
    // link.click();
    saveAs(canvas.toDataURL("image/jpeg"), 'instantane.jpg');
  });

  document.documentElement.classList.remove("hide-scrollbar");
  options.classList.remove("hide");
});


function saveAs(uri, filename) {
  const link = document.createElement('a');

  if (typeof link.download === 'string') {
    link.href = uri;
    link.download = filename;
    link.target = '_blank';
    document.body.appendChild(link);
    link.click();
    //remove the link when done
    document.body.removeChild(link);
  } else {
    window.open(uri);
  }
}

// Canvas p5.js

const optionsHeight = document.querySelector('.options').clientHeight;
const eraserBtn = document.querySelector('.button--eraser');
const plusBtn = document.querySelector('.button--plus');
const minusBtn = document.querySelector('.button--minus');
let eraserToggle = false;
let crayon;
let gomme;
const brushSize = 10;
const originalPenSize = 2;
let penSize = originalPenSize;
const originalEraserSize = 4;
let eraserSize = originalEraserSize;

let cursorSize = 4;
let cursorBgSize = 8;
cursor.style.width = cursorSize + "px";
cursor.style.height = cursorSize + "px";
cursorBg.style.width = cursorBgSize + "px";
cursorBg.style.height = cursorBgSize + "px";

eraserBtn.addEventListener('click', (e) => {
  e.preventDefault();
  eraserBtn.blur();
  if (menuOpen) {
    menuOpen = !menuOpen;
    menu.classList.remove("show--menu");
  }
  if (!eraserToggle) {
    // eraserBtn.style.background = "#000";
    // eraserBtn.style.color = "#fff";
    // eraserBtn.querySelector('svg').style.fill = "#fff";
    cursor.classList.toggle('cursor--eraser');
    cursorBg.classList.toggle('cursor--eraser');
    eraserBtn.classList.add('button--toggle');
    eraserToggle = !eraserToggle;
  } else if (eraserToggle) {
    // eraserBtn.style.background = "#fff";
    // eraserBtn.style.color = "#000";
    // eraserBtn.querySelector('svg').style.fill = "#000";
    cursor.classList.toggle('cursor--eraser');
    cursorBg.classList.toggle('cursor--eraser');
    eraserBtn.classList.remove('button--toggle');
    eraserToggle = !eraserToggle;
  }
});

plusBtn.addEventListener('click', (e) => {
  e.preventDefault();
  eraserBtn.blur();
  if (menuOpen) {
    menuOpen = !menuOpen;
    menu.classList.remove("show--menu");
  }
  // if (eraserToggle) {
    eraserSize = eraserSize + 2;
  // } else {
    penSize = penSize + 2;
    cursorSize = cursorSize + 2;
    cursorBgSize = cursorSize * 2;
    cursor.style.width = cursorSize + "px";
    cursor.style.height = cursorSize + "px";
    cursorBg.style.width = cursorBgSize + "px";
    cursorBg.style.height = cursorBgSize + "px";
  // }
});

minusBtn.addEventListener('click', (e) => {
  e.preventDefault();
  eraserBtn.blur();
  if (menuOpen) {
    menuOpen = !menuOpen;
    menu.classList.remove("show--menu");
  }
  // if (eraserToggle) {
  //   if (eraserSize > originalEraserSize) {
      // eraserSize = eraserSize - 2;
    // }
  // } else {
    if (penSize > originalPenSize) {
      eraserSize = eraserSize - 2;
      penSize = penSize - 2;
      cursorSize = cursorSize - 2;
      cursorBgSize = cursorSize * 2;
      cursor.style.width = cursorSize + "px";
      cursor.style.height = cursorSize + "px";
      cursorBg.style.width = cursorBgSize + "px";
      cursorBg.style.height = cursorBgSize + "px";
    }
  // }
});

// function preload() {
//   crayon = loadImage('etc/brush.png');
//   gomme = loadImage('etc/eraser.png');
// }

function setup() {
  const canvas = createCanvas(windowWidth, windowHeight);
  canvas.parent('contenu');
}

function draw() {
  if (mouseIsPressed && !menuOpen) {
    // if (mouseY < (windowHeight - optionsHeight)) {
      if (eraserToggle) {
        // imageMode(CENTER);
        // image(gomme, mouseX, mouseY, brushSize, brushSize);
        stroke(255,255,255);
        strokeWeight(eraserSize);
        line(pmouseX, pmouseY, mouseX, mouseY);
      } else if (!eraserToggle) {
        // imageMode(CENTER);
        // image(crayon, mouseX, mouseY, brushSize, brushSize);
        stroke(0,0,0);
        strokeWeight(penSize);
        line(pmouseX, pmouseY, mouseX, mouseY);
      }
    // }
  }
}

// function mouseDragged() {
//   strokeWeight(2);
//   line(pmouseX, pmouseY, mouseX, mouseY);
// }

clearBtn.addEventListener('click', (e) => {
  e.preventDefault();
  clearBtn.blur();
  if (menuOpen) {
    menuOpen = !menuOpen;
    menu.classList.remove("show--menu");
  }
  clear();
});

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}

Affiche(true);

