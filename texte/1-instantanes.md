```{=tex}
\cleardoublepage
\thispagestyle{empty}
\vspace*{9\baselineskip}
\epigraphe{Le fait de tracer des inscriptions, des signes ou des dessins, sans autorisation préalable, sur les façades, les véhicules, les voies publiques ou le mobilier urbain est puni de 3\,750~euros d'amende et d'une peine de travail d'intérêt général lorsqu'il n'en est résulté qu'un dommage léger.}{Art. 322-1 al. 2\\du Code pénal français}{}
\cleardoublepage
\begin{nsright}
\itshape
\thispagestyle{empty}
\vspace*{9\baselineskip}
Toujours sur la brèche\\
J'pense à BB Brecht
\end{nsright}
```
# Manifesto

```{=tex}
\begin{nscenter}
\itshape
```
Ceci est une invitation,\
le manifeste d'une coterie.\
Celle des parjures, celle des profanateurs,\
celle de ceux qui se sont évadés\
et qui se retrouvent sur la plage.

```{=tex}
\end{nscenter}
```
## De notre forme de vie

### Association de bienfaiteurs en bandes organisées

Nous appelons à l'auto-organisation des conseils, des groupes, des conciliabules et des coteries. Dans l'ombre, dans le noir, dans la nuit. Et nous ne reviendrons tout niquer que sous l'égide d'Athéna.

### Mon binôme et moi comme Gilgamesh et Enkidu

Il est moi et je suis lui. Oublions nos noms, inventons en d'autres. Nos identités sont souples, multiples et déterminées par nos actes.

### Doux casseurs

Parce que la violence est nécessaire, la destruction aussi, mais jamais systématique. La violence est source d'énergie. L'énergie est nécessaire à la transformation et au changement. Forcer un état de stase, comme le fait l'État pour conserver l'ordre social, nécessite aussi de l'énergie, énergie qui n'est pas générée sans violence. La stase est donc elle aussi violente. La violence est inévitable : soit nous la subissons, soit nous l'utilisons pour faire changer les choses. Reste à savoir contre quoi et à quoi nous l'employons et ce que nous pouvons faire sans elle. Elle n'est en aucun cas négligeable.

### Nous voulons un squat avec vue sur la grève

Est-ce un rêve, est-ce un but, est-ce les deux ? C'est ce à quoi nous aspirons, d'abord parce que vendre de l'espace à habiter doit être considéré comme un crime. L'espace n'est pas à vendre et encore moins à louer, habiter n'est pas une lubie, c'est une dignité irrévocable et inaliénable sans laquelle nous nous excluons de l'humanité. Même les fourmis ont des fourmilières, sans pour autant payer de loyer, les coraux créent eux-mêmes leur habitation et celles de nombreuses autres espèces en bâtissant avec leur propre corps des assemblages sous-marins servant d'abris, pourtant nous ne voyons ni notaire ni agent immobilier sous la mer (même si nous désirons à ce qu'ils y terminent). Nous aspirons simplement à ne plus avoir froid, à pouvoir dormir et nous reposer sans avoir à penser nos vies comme un exercice comptable. Habiter n'est pas un service, c'est un dû. Quiconque en profite n'a plus à être considéré comme un membre digne de notre espèce. Nous voulons nous réveiller, prendre un verre d'eau et regarder par la fenêtre pour voir des amoureux qui s'embrassent, ici des enfants qui ne sont pas à l'école et là ouvrières et paysannes qui discutent en haranguant la foule sur la nécessité de tels ou tels travaux dans la rue pendant que sur les murs du bâtiment abandonné, des peintres-vandales font leur office et que, sur la place au bout de la rue des livreurs à vélo préparent un échafaud pour des flics et des banquiers récalcitrants. Est-ce trop vouloir ? Nous ne pensons pas.

## De nos manières

### Avant de faire l'amour à la plage, il faudra bien retirer les pavés

Les vieilleries ont quelques avantages, aussi faut-il bien en conserver quelques-unes pour en rire et se souvenir qu'avant nous d'autres ont essayé, et que c'est maintenant à notre tour d'avoir à justifier nos méthodes et nos formes de vie. Tant pis, nous le ferons parce que nous n'avons pas le choix. De nos luttes nous ne sommes pas libres tant elles sont viscéralement inscrites en nous. À quoi bon nous soustraire, à quoi bon rentrer dans ce spectacle. Nous n'en voulons pas et nous le ferons savoir en tout lieu et en toute occasion, quitte à dépaver nos rues, nos villes et même le monde entier.

### Ultragauche mais super adroite avec un pavé

La vraie générosité envers l'avenir consiste à tout jeter aux fascistes (et donc, aussi, aux policiers).

### L'écriture est automatique, l'AK-47 aussi

Où sont les sten-o-graphes ? où sont les poètes-guerriers ? Si mort est l'engagement, alors nous sommes des nécromanciens. L'engagement sans idéal n'est que mercenariat. L'idéal sans engagement n'est qu'une posture. Nous appelons à la fin des deux. Nous avons des revendications et les moyens de les faire vivre. Notre refus est intellectuel et corporel, les deux sont nécessaires, puisqu'ils ne sont qu'un : nous n'appelons pas à la fin des fausses divisions par esthétisme. Par les mots, nous évoquons le monde tel que nous le voulons, nous sommes des démiurges répondant à l'impérieux appel de l'utopie à elle-même.

### Aujourd'hui de peinture, demain artisanale

Est-ce encore un jeu ? Une dernière partie avant le catéchisme, parce que nous sommes encore des enfants, parce que nous avons décidé de le rester et de ne pas rentrer dans cette valse morbide. Alors, pour quelque temps encore, nous lutterons par la splendeur des matins, parce que nous y aspirons. Mais nous ne sommes pas dupes, des enfants, nous avons perdu la naïveté première et, loyaux avec la vie, nous savons où nous nous dirigeons et, dans la nuit noire, nous conjurons, nous sabotons.

### Les tags s'effacent, la verve reste

Les travailleurs précaires sont envoyés pour nous faire taire. N'oublions jamais que ces personnes ne sont pas pleinement libres, qu'elles restent soumises à la domination de leur employeur. C'est à nous de réitérer. La verve reste.

### Vandalisme et Volupté

Parce que le vandalisme n'est pas une dégradation, car sinon tous les imbéciles en voudraient au temps qui passe. Tout se dégrade, se transforme en se délitant. Nous, nous vandalisons, nous redonnons au beau sa place, c'est-à-dire dans les rues, les corps et les esprits. C'est ça être un vandale, c'est pervertir, suriner et repousser le laid et l'idiot. Les vandales sont des rhapsodes.

## De notre poésie

### La prose est à la poésie ce que l'aliénation est à la volupté

La poésie est émancipatrice et pour être efficace elle doit être subversive. Il faut donc faire de la poésie de nos vies, faire de la musique de nos cris, montrer que d'autres manières sont possibles. "Tous les arts contribuent au plus grand de tous les arts, l'art de vivre." L'art de la situation, voilà ce dont nous ressentons cruellement le manque.

### Kalos kagathos : jusqu'ici tout est laid

Aussi, rien n'est bon, et faire croire qu'une vie bonne peut se faire dans cet environnement est un mensonge criminel. Aussi, pour réfuter le mensonge, nous devons dire le beau, mais aussi l'écrire, le chanter, le jouer et l'être.

### Tout est inique, alors on nique tout

Tout est à refaire, car de partout le beau et le juste semblent avoir été écartés, et pour refaire, il faut oublier le passé et ses laideurs. Tout niquer devient vital. Nous n'avons plus le choix, le laid, l'injuste et la petitesse doivent mourir, quitte à ce que nous en soyons les assassins.

### Il n'y a de guerre plus réelle que celle des imaginaires

Narrer les possibles, redonner aux faisables une couleur. À chaque fois que nous entendrons un "ça pourrait être pire", nous répondrons "ça pourrait être bien mieux", chaque "c'est comme ça et pas autrement" sonne en nous comme un défi.

### C'est dans le théâtre de nos esprits que se joue le Lac des signes

Nous refusons le réel, sa petitesse, sa laideur et sa saturation pécuniaire de comptable minable. Un jour nous entendons "je t'ai donné la vie" et l'autre "maintenant, tu dois la gagner". Refusons ces adages imbéciles. Vivons en dehors de ce système marchand et mortifère. Pas de rapports marchands entre nous. Instaurons et faisons proliférer de nouvelles manières. Si nous ne pouvons montrer au monde que c'est possible, faisons au moins vivre cet idéal entre nous.

## Le Zeitgeist

### Nous rêvons du Grand Soir comme au Songe d'une nuit d'été

Nous savons que c'est un songe, mais nous ne pouvons nous soustraire à l'utopie.

### Nous venons faire l'Edda des lieux

Parce que les villes n'ont plus d'histoire à nous raconter, alors nous raconterons les nôtres sur leurs murs. Belles, lyriques, épiques, criminelles, drôles et humaines, voilà ce qu'elles seront.

### Les précaires rêvent-ils de moutons ubérisés ?

La précarité n'est pas seulement financière comme *on* voudrait nous le faire croire. Multiples sont ses formes. Précarité amicale, sentimentale, menstruelle ou idéologique, la précarité est devenue une forme de vie banale, une crise permanente, la ruine de l'âme, nous empêchant de contempler sereinement notre avenir. Nos solitudes comme un grand désert : il est grand temps de nous sortir des sables mouvants. K. Dick voulait savoir si les androïdes aspiraient à la beauté, elles et eux, mi-machines, mi-humains. La question se pose aussi pour les membres du précariat : à quoi aspirent-ils ? C'est un crève-cœur que de comprendre cela : les nexus voulaient vivre, aimer, s'émanciper de leurs créateurs et c'est exactement ce qu'*on* refuse aux précaires. N'est-ce pas là suffisant pour se révolter et en finir avec les monologues sous la pluie ?

### Notre start-up, c'est le zbeul

\noindent Notre open-space, c'est la rue.

\noindent Notre process, c'est l'anarchie.

\noindent Notre produit, c'est l'émancipation des peuples et des individus.

\noindent Nous venons disrupter le discours dominant, la société du contrôle, l'ennui et l'État.

### Donner ses données, google t'a volé

L'usurpation est un crime. Qu'essaie-t-*on* de faire en récoltant toutes les informations possibles sur ta manière de te comporter ? *On* essaie de te substituer, de te réifier par les données que tu auras produites. Le capitalisme n'a plus besoin de toi, seulement de tes données. Ton esprit, ton corps, ton individualité ne sont plus nécessaires à son fonctionnement. Des chiffres, toujours plus de chiffres pour faire des modèles, des statistiques, des prévisions. Les êtres n'auront plus besoin de substance, tous les modèles de comportements possibles et imaginables seront prêts, il n'y aura plus qu'à choisir quelle forme de vie policée et adaptée à la consommation *on* préfère. Que crève cette société du contrôle, de la prévision. La mathématique n'épuisera jamais le réel, nous y veillerons.

### La liberté d'entreprendre, c'est la liberté d'exploiter

Nous refusons toute forme de subordination. Ni esclavage, ni servage, ni salariat. Quelle différence entre dire qu'il faut bien travailler à l'école pour avoir un bon métier, une bonne vie et "le travail rend libre" ? Le sinistre cynisme n'a plus sa place. La liberté et le libéralisme économique sont deux choses différentes, corrélées, certes, la première étant l'inspiration de la seconde, mais jamais la liberté d'entreprendre n'épuisera l'idée de liberté et le croire est une insulte à la fois à l'idée de liberté et au genre humain. Et c'est avec cette insulte qu'une partie du corps politique ose se présenter à nous ? La dépolitisation du travail et du salariat n'est qu'une arme pour affaiblir le prolétariat, et sa transformation en précariat n'est alors plus une surprise. Les belles idées ne peuvent produire de la misère sociale, et si le libéralisme économique, grimé en liberté primordiale, aggrave la condition des travailleurs, c'est qu'alors il faut la combattre. L'odieux n'a plus sa place.

## Quelques conseils entendus dans les ziggourats de Baal

```{=tex}
\setlength{\parindent}{0em}
```
Ne pas confondre Entropie, Chaos et Destruction

```{=tex}
\vspace{1\baselineskip}
```
Se laisser abattre ne sied qu'aux arbres

```{=tex}
\vspace{1\baselineskip}
```
Le constructivisme, c'est l'importance des idées

Le matérialisme, c'est l'importance des structures

Les deux sont nécessaires.

```{=tex}
\vspace{1\baselineskip}
```
Les pavés sont semblables aux oiseaux

Toujours de bon augure

Lorsque dans le ciel, ils volent haut.

```{=tex}
\vspace{1\baselineskip}
```
Déchire la nuit

Transcende le jour

Qu'importent les voiles

Qu'importent les augures.

```{=tex}
\vspace{1\baselineskip}
```
Abjure,

Parjure,

Conjure.

Dans cet ordre.

```{=tex}
\vspace{1\baselineskip}
```
Profane la marchandise

Détruis ses temples

Tue son clergé

Mais pardonne à ses fidèles.

Car si la critique de la consommation est nécessaire, la critique du consommateur est une idiotie.

```{=tex}
\vspace{1\baselineskip}
```
Tu n'as rien à déclarer. Tu n'as rien à déclarer. Tu n'as rien à déclarer. Tu n'as rien à déclarer. Tu n'as rien à déclarer. Tu n'as rien à déclarer. Tu n'as rien à déclarer. Tu n'as rien à déclarer. Tu n'as rien à déclarer. Tu n'as rien à déclarer.

```{=tex}
\vspace{1\baselineskip}
```
Ton seul crime est d'avoir voulu vivre une vie digne, saine et belle.

```{=tex}
\vspace{1\baselineskip}
```
N'êtes-vous pas fatigués d'être les comptables de votre propre existence ?

```{=tex}
\vspace{1\baselineskip}
```
Nos désirs font désordre.

```{=tex}
\vspace{1\baselineskip}
```
La différence entre le possible et le faisable, c'est l'intelligence. Elle est donc primordiale et la définir serait une insulte.

```{=tex}
\setlength{\parindent}{1em}
```
```{=tex}
\cleardoublepage
\begin{nsright}
\itshape
\thispagestyle{empty}
\vspace*{9\baselineskip}

Ici tout est noir\\
À l'ouest, l'océan couleur de fer\\
À l'est, les puits de pétrole de Babylone\\
Au nord, il y a Kadath\\
Et au sud, le grand désert\\
Mais ici tout est noir\\
Et je ne marche que dans la nuit\\
La nuit qui n'en finira plus

\end{nsright}
```
# De la fonction idéologique du vandalisme

Trop souvent le raccourci est fait : le vandalisme ne sert à rien, la dégradation est un manque de respect aux biens publics et dessert celles et ceux qui l'utilisent le plus. Ainsi, les voyous commettant ces actes sont au pire des criminels, au mieux des faquins (aussi dois-je l'avouer, souvent, l'autonomiste se place dans cet entre-deux). Mais il y a une raison à cet acte de vandalisme. Oui, il est gratuit, à condition qu'*on* le prenne seul et qu'*on* refuse de voir l'entièreté de la pratique du geste et de ceux qui l'exécutent, car ce geste n'est jamais dénué d'un but, d'un sens. Refuser de le voir, c'est se rendre aveugle à un combat, celui des idéologies.

## L'idéologie dominante, la division

Alors, revenons premièrement sur l'idéologie dominante. Je ne parle pas ici de politique, mais bien d'un ensemble d'outils, de valeurs et de traits culturels qui façonnent les esprits et nos modes de pensée (c'est-à-dire nos manières de voir). C'est bien dans ce cadre que la politique se joue : en dehors de la structuration qu'impose la question "Comment diviser les richesses produites par les nations ?", rien d'autre n'est discuté. L'entièreté du jeu politique tourne autour de cette question de répartition qui divise la politique française (et plus largement mondiale). Le jeu du pouvoir n'est, dès lors, qu'un sous-espace du cadre idéologique des dominants. Jouer sur l'idéologie, c'est aussi jouer sur le politique.

Or, quelle est l'idéologie dominante dans nos sociétés ? Quelle est la logique fondamentale qui structure nos sociétés ? C'est la logique de division : celle du travail, de la hiérarchie, celle des genres et des identités aussi. Prenons comme exemple simple une maison : dans la logique même de sa conception, *on* a divisé l'espace par fonction : ici, *on* prépare à manger, *on* appelle cet espace la cuisine, ici *on* y mange, c'est la bien nommée salle à manger, et là, nous dormons, c'est la chambre. C'est bien la division de l'espace (par fonction) qui nomme, norme et définit l'espace. Il en va de même pour la politique (pour reprendre notre exemple précédent) : celles et ceux qui pensent que les richesses produites doivent être réparties équitablement sont "à gauche" et les autres, qui pensent que la majeure partie doit revenir aux détenteurs des moyens de production, sont "à droite". Nous noterons que la question initiale ou que la distinction entre les deux groupes aurait pu être tout autre, mais il faut faire une distinction, il faut faire une division. Il faut séparer les groupes en fonction de cette question et de ces réponses. Il y a plein d'autres exemples, observe n'importe quel organigramme d'une société. La division structure nos manières de faire, de voir, de penser.

## Comment combattre une idéologie ?

Alors quel est le lien entre la division comme idéologie et ces quelques mots peints ou écrits sur des murs, des tables, des chaises ? Nous y venons. Rappelons-nous Sartre quand il prend l'exemple du coupe-papier ; il existe un concept de coupe-papier qui définit une certaine utilité, une fonction précise de cet objet. C'est son essence, ce qu'il est. C'est le point commun de tous les objets. Nous les créons pour qu'ils puissent remplir une fonction. Les murs ont une fonction séparatrice (ou porteuse, parfois les deux). Les tables sont des espaces de travail et les chaises ont pour fonction de nous permettre de nous asseoir, un peu n'importe où. Bref, un objet répond à un rôle qu'*on* lui affecte lors de sa conception. Un objet répond à un besoin. Où que nous soyons, nous pouvons regarder autour de nous et nous demander à quoi servent ce truc, ce machin et ce bidule-là ? Aussi, il devient intéressant de se demander si l'inverse est possible : y a-t-il un objet qui ne sert à rien ? Une œuvre d'art ? Un tableau ? C'est oublier la fonction esthétique et culturelle de ces objets-là, elles ne sont pas indispensables, même si leur absence rendrait le monde bien triste, mais laissons là les considérations anthropologiques, ce n'est pas le sujet ici. Trouver un objet dénué de toute fonction est plus que difficile. Chaque objet est donc une partie d'un grand tout, organisé et divisé en fonctions, en tâches, en manières. Alors que faire de tous ces objets fonctionnels ? Rien ! Qui serait assez fou pour salir, dégrader, vandaliser un élément de cette formidable machine organisée que forment notre société et ses champs. Et c'est là que se noue le nœud : l'idéologie est invisible, exactement comme les lunettes le sont aux hypermétropes. Il n'y a, semble-t-il, aucun intérêt au vandalisme. À moins que ce ne soit pas à l'objet lui-même que nous (et là je m'inclus volontiers dans ce nous) nous attaquions, mais bel et bien à l'idéologie qui a permis sa production, sa conception et sa raison d'être. Nous n'avons que faire des murs, des tables et des chaises en réalité, nous en voulons à l'unicité de leur fonction et à l'idéologie qu'ils diffusent passivement (et dont ils sont les complices).

## Le vandalisme est un moyen, pas une fin

Encore une fois, être aveugle à l'idéologie dominante (ce qui est complètement admissible et recevable puisque nous baignons depuis toujours dedans), c'est se rendre sourd aux méthodes de celles et ceux qui la combattent. Alors oui, des rabat-joie, nous dirons que les idéologies se combattent à coups de grandes idées, de grands discours dans des assemblées représentatives. Oui, peut-être. Mais est-ce le seul lieu où peut se dérouler la guerre des idéologies ? Doit-on laisser cette lutte primordiale (ici primordiale est le mot le plus juste : de cette lutte découlent toutes les autres) à des gens déjà issus d'un sous-produit de l'idéologie dominante ? Nul système de gouvernance ne devrait laisser ses membres être juges et parties. L'idéologie que les autonomistes appellent de leurs vœux, et moi avec, ne sera jamais combattue dans un quelconque hémicycle et sera nécessairement vue comme un péril mortel (ou du moins un délire quelconque) par n'importe quelle belle âme de la cité tant que l'idéologie dominante prospérera. Alors, où combattre ? Partout ! Partout la lutte doit s'inscrire : dans chaque événement, dans chaque lieu, partout, ici et maintenant ! Sur les murs, les tables, les chaises, mais aussi et surtout, dans les corps et dans les esprits, là où se joue le véritable Lac des signes. En fait, pervertir un mur ne doit pas être vu comme une dégradation, même le plus pur des diamants taillés se change lentement en simple graphène sous l'effet du temps : tout se dégrade, *tout se transforme*, partout et tout le temps. L'argument de la dégradation ne tient pas : la simple transformation ne peut pas être le sujet du rejet. Alors pourquoi une telle haine bourgeoise du vandalisme ? Parce que la dégradation, c'est cette injure à la fonction, une injure à la sacro-sainte division policée, admise, à cette doxa. Le vandalisme est un parjure idéologique. Vandaliser, c'est dépasser la doxa, avec des outils aussi simples et idiots qu'un feutre, une bombe de peinture ou des affiches, c'est dire : "Je décide que cet objet n'est pas que cet objet tel qu'il a été pensé et conçu, maintenant, c'est un support de ces quelques mots, de ma poésie ou de celle d'un ou d'une autre." Appliqué à un mur d'un commissariat, d'une prison ou d'une préfecture, et vous briserez le tabou de l'immuabilité de ces institutions. Voilà le vrai danger du vandalisme : en ajoutant une fonction poétique à n'importe quel objet, nous souhaitons briser cette vision idéologique, remettre en question la fonction des choses, instiller du doute directement dans l'objet et sa fonction. Nous parasitons. C'est un acte subversif, aussi il doit nécessairement déranger et c'est bien à cela que l'*on* mesure son effet. Rien de plus, rien de moins, c'est une manière de lutter, par le parjure, la moquerie, c'est un coup de surin dans les imaginaires. Ce n'est ni grandiose ni beau, mais le décalage qu'il permet se veut comme un trou d'air vers un ailleurs idéologique.

## Provoquer l'hémorragie idéologique 

Avouons-le, ce n'est pas un tag qui déclenchera la révolte populaire, c'est certain, mais disons qu'il permet de s'insinuer dans les esprits et dans l'imaginaire de parfaits inconnus, des passants et des passantes et c'est déjà suffisant. Il n'est pas donné à tout le monde de laisser une trace, aussi fugace soit-elle. Considérons simplement le vandalisme pour ce qu'il est, c'est-à-dire un moyen de lutter comme un autre. Encore une fois, la lutte est ici idéologique, et non pas politique, ou alors que par écho, par hémorragie. L'ennemi n'est pas la droite, ni même l'extrême droite, mais celles et ceux qui prônent la division, la hiérarchisation (mais aussi la marchandisation de tout). Vandaliser, c'est refuser cette idéologie, et le dire à tout le monde. Vandaliser, c'est dire merde avec un peu d'élégance, et comme toute forme d'élégance, elle est inutile. Et c'est bien ça que nous recherchons. Une forme non conventionnelle de crier, de faire tourner les regards, ne serait-ce que quelques secondes sur un artefact, une anomalie. Créer un bug dans la matrice (idéologique), un affect, une sensation, sortie de nulle part, n'ayant pas de sens. De la poésie ex nihilo, partout.

```{=tex}
\cleardoublepage
\begin{nsright}
\itshape
\thispagestyle{empty}
\vspace*{9\baselineskip}

Dans la nuit comme Noctali\\
End the patriarchy\\
Ballade entre les étoiles avec Satie\\
Smash the binary\\
J'ai lu l'avenir dans le prisme\\
Death to capitalism\\
J'aimerais être enterré au Liban\\
Hail Satan

\end{nsright}
```
# Fragment d'un journal égaré

```{=tex}
\vspace*{-1\baselineskip}
```
## Hallalis

J'avais parié sur 1984, je me suis trompé. C'est Alan Moore qui va gagner. Tant mieux, je préfère les capes et les dagues aux calculs mathématiques erronés. Ils, je parle ici de nos rats tant détestés, ont profité du confinement pour raser la ZAD de la Dune, à Brétignolles. Je suis, je l'avoue, sur le cul. Ça m'étonne : je peux éprouver de la sympathie pour les petits animaux musqués, leurs petites moustaches me les rendent parfois agréables. Mais je n'éprouve que de la haine, de la rancœur et du mépris pour ces *eux-là*. Ce ne sont donc pas des animaux, je me suis trompé. Ils sont, logiquement, en dessous du règne animal. Peut-on être spéciste, mais qu'avec les forces de l'ordre ? Il y aurait le vivant, et les forces de l'ordre en dessous. C'est pour moi la meilleure classification possible de la biosphère terrestre. Mumu, nationale, CDI, BRAV, CRS, BAC et tous les autres que j'oublie, parce qu'oubliables, je vous pisse au cul, du piédestal du vivant d'où je me trouve. Là où les LBD ne peuvent nous atteindre. Vous n'êtes ni des êtres humains, ni des animaux, ni des insectes, ni des champignons, ni des arbres, ni des plantes, ni même des microbes. Vous êtes de gros tas de merde. De gros tas de merde dangereux pour le vivant. Aussi, nous sommes vos antagonistes et nous vous détruirons comme vous avez détruit la ZAD. Œil pour œil, préf pour ZAD. Et ne vous inquiétez pas, en braves golems de merde que vous êtes, nous vous renverrons ceux qui vous servent de maîtres dans la foulée.

## Libations

Où se cachent les hiérophantes ? Quand je me rappelle à eux, la saudade m'envahit, alors je me souviens ne jamais les avoir rencontrés. C'est impossible. Il ne me reste plus qu'à déambuler dans ces rues froides, dans cette nuit qui ne finit pas. N'ai-je connu le jour au moins une seule fois ?

Peut-on être la tumeur d'un ensemble déjà sclérosé ? Cette pensée limitée par Boole me rend malade. Mal imaginaire qui, moins par moins, donne encore moins. Cette logique est facilement dépassable : *la mathématique n'épuise pas le réel*. (Et pourtant tu te transformes en BigGrosTas.)

Sévère retour de bâton. Des millénaires durant, quelques hommes ont utilisé, enrichi et développé l'algèbre et la géométrie pour organiser la vie de leurs semblables. C'était un outil, comme un gant mental pour saisir ce fer-monde rougeoyant. Notre histoire est donc celle du Midas qui, un jour, décida de se masturber.

```{=tex}
\cleardoublepage
\begin{nsright}
\itshape
\thispagestyle{empty}
\vspace*{9\baselineskip}

Dis-moi quand le jour redeviendra\\
Cette danse macabre sur le mont chauve\\
N'a déjà que trop duré\\
Au Diable les autonomes\\
Au Diable les antifas\\
Lucifer, je pense à toi

\end{nsright}
```
# Prolégomènes de l'In-Situ-Arsonisme

*Tout niquer seul, j'en ai jamais rêvé.*

## De sa nécessité et de son origine

Toute forme de réinterprétation du concret, du réel est dangereuse. Soyons dangereux.

Si dépolitiser le réel, c'est le repolitiser au profit de l'oppresseur, pourquoi ne pas simplement politiser le réel ? Cette opération sera toujours à notre avantage. Repolitiser le réel, c'est être dangereux.

Toute situation est une repolitisation du réel. Toute situation est dangereuse. Si une situation n'est pas dangereuse, alors ça n'en est pas une et elle n'aurait donc aucun intérêt.

Il faut donc inventer une pensée du danger : l'in-situ-arsonisme. Le danger, c'est la capacité de dépasser les déterminismes présents, ici et maintenant.

## Le syncrétisme des syncrétismes

Miroir de la détermination des déterminations, le syncrétisme des syncrétismes se veut comme un danger.

Le syncrétisme, à l'origine, est la fusion des imaginaires, des dieux et de leurs histoires dans le but de produire un nouveau corpus de mythologies. Comme un jouet inoffensif, le syncrétisme est un outil de petits nervalos. C'est une fusion de l'immatériel, à l'intérieur de lui, en lui, dans le but de le revitaliser, d'en faire un nouvel objet, ayant la possibilité d'alimenter le spectacle. Ce syncrétisme-là est mort.

Le syncrétisme des syncrétismes a pour but de fusionner l'ensemble des histoires. Vécues et non vécues, réelles et imaginaires, personnelles et sociales. C'est une fusion de toutes les formes de narration. Ici, nous mélangerons toutes les histoires, non pas pour en faire un récit monolithique et immuable, mais, au contraire, en faire des fragments en eux-mêmes, mais miscibles entre eux. Le syncrétisme des syncrétismes vise à fournir un ensemble d'outils artistiques et politiques dangereux, car permettant, par le mélange des imaginaires et des réalités, de nouveaux discours de réinterprétation subversive du réel.

Cette fusion épuise à la fois le concept de réel et d'imaginaire. Fusion totale et englobante. Dès lors, dans cet espace, la limite entre les deux disparaît. Le Texte ainsi est transréel, transréalités et transimaginaires. Dans cette transcendance, ce mélange, cette complexité se trouve le danger. Une histoire n'est qu'un sous-espace à l'intérieur de l'espace du syncrétisme des syncrétismes.

Ce décalage-dépassement est nécessaire pour faire renaître l'idée de l'aventure, idée oubliée depuis trop longtemps déjà. Non, les fables du néolibéralisme : la start-up, tes quelconques romances, les élections présidentielles et les soldes ne sont pas les seuls événements qui pourraient t'arriver. Ce triptyque n'épuise pas le concept d'aventure, voilà une vérité qu'il faut raconter à nouveau. Et si raconter l'aventure est un danger, la vivre l'est aussi.

## L'aventure

Dans les grands récits, du plus ancien au plus récent, le personnage principal devient le héros grâce à son courage, sa force ou son intelligence, il se transcende, devient quelqu'un grâce à un don, une maîtrise particulière de telle ou telle capacité magique, héritée ou dont il est le récepteur unique. À force d'épreuves, il devient capable, enfin, d'affronter le *Mal incarné* qui oppresse le village, le pays, le monde ou même la galaxie. Son retour est alors triomphal, parti en enfant ou en adolescent, il revient en héros, en brave parmi les siens. L'aventure a fait de lui un homme, un performé, un archétype (pour ne pas dire un cliché), un rôle social type. Cette histoire est vieille comme le monde, littéralement, et elle est même parvenue jusqu'à nous avec l'avènement de la *start-up nation* et de l'apparition de cette nouvelle classe sociale dont il a bien fallu peupler l'imaginaire et alimenter la croyance : l'(auto-)entrepreneuriat. Voilà l'aventure devenue farce. Remplacez la menace initiale par le chômage, notre *enfant* par un gars un peu moyen, sans moyens. Grâce à son travail, son envie, son énergie, il réussira à monter sa start-up, après avoir surmonter de nombreuses embûches et il deviendra un jour à son tour un mentor, un expert de l'expertise adoubé par les prophètes-investisseurs. Ainsi armé, il pourra enfin combattre ces terrifiants symboles du temps ancien, ces grands groupes industriels polluants, aux côtés des GAFAM nimbés de lumière. La titanomachie n'est jamais vraiment loin. La farce néolibérale est d'avoir fait croire, religieusement, que la seule aventure possible dans nos vies était celle de la start-up et de la réalisation de soi par le travail. La passion au service du capital pour enfin devenir libre et libérer les siens du terrible chômage et du désastre climatique et écologique (rayez la mention inutile). Devenir libre, ici, c'est devenir un membre de la classe dominante. Ici la seule liberté, c'est celle de dominer les autres. À vous de choisir : ne rien faire et subir, ou devenir un héros en traversant la route et les terribles épreuves de la jungle (économique) pour finir employeur : clé vers l'au-delà.

L'aventure ne se résume aucunement à cette farce insipide. L'amour et la réussite entrepreneuriale sont, au mieux, de mauvaises histoires, au pire, un opium comme un autre. Il existe des millions d'aventures, qu'elles soient à vivre ou à imaginer. Chopez-vous des bombes et allez taguer votre centre-ville avec trois, quatre potes, une nuit et le lendemain, vous rigolerez de voir vos exploits perturber les passants, ou peut-être que vous passerez la nuit au poste. Et alors ? Est-ce vraiment important ? Plus simple et plus ludique : à la fin des marchés, récupérez quelques invendus après avoir aidé les maraîchers à ranger leurs étals. Avec les invendus, et toujours quelques amis, faites la cuisine pour 20 ou 30 personnes dans une grosse casserole, en ajoutant du riz ou des lentilles. Enfin, préparez de quoi manger, des assiettes et des couverts de récupération, une table si nécessaire, et installez-vous quelque part en ville, un peu avant midi. Offrez un repas gratuit à des inconnus, écoutez-les, écoutez-les raconter leurs aventures tout en vivant la vôtre. Il en existe tellement, pourquoi s'en priver ? Être un pirate est à la portée de toutes et de tous. Dans la rue, dans les universités, dans les squats, à la ville comme à la campagne : partout. Partout sauf, peut-être, étrangement, au travail. Peut-être est-ce cela la servitude : la vie sans aventure, ou plutôt où toute aventure sert le capitaliste, à nos propres dépens. Ce qui fait de la grève la première aventure du travailleur sur son lieu de travail, et c'est bien en cela qu'elle est dangereuse. Non pas parce qu'elle permet un changement concret et matériel (qui reste souhaitable), mais parce qu'elle féconde l'imaginaire d'une autre temporalité et d'un autre rapport social au travail. La grève est, même si elle échoue, une aventure poétique. Il en existe plein d'autres, certaines doivent être tues, d'autres peuvent être racontées. Dans les deux cas, il faut les vivre.

## L'idéologie et le politique

L'entièreté de la question politique se résout dans le système capitaliste. Il en va de même pour le socialisme : la solution, cette solution n'existe que pour et par le capitalisme. Le socialisme n'est qu'une forme de capitalisme. L'entièreté du spectacle politique est de nous faire croire à la viabilité d'une forme sur l'autre : la gauche et la droite.

Le projet révolutionnaire doit se faire en dehors de ce faux cadre idéologique, celui imposé par la bourgeoisie. Toute promesse qui en resterait à l'intérieur ne serait que mensonge et diversion : contre-révolutionnaire. Or la doxa est partout. La contre-révolution est donc partout. La révolution doit être non capitaliste, non bourgeoise, la négation pure et simple de la doxa.

Que contient la négation ? Si le capitalisme se définit par l'idéologie cherchant l'accumulation toujours plus large de capitaux (sous toutes ses formes, de plus en plus diverses), alors nous pouvons le nier de plusieurs manières : l'une, qualité par qualité, contenue dans cette matrice ou en dehors d'elle, l'autre, pour ce qu'il est, pour son essence, son esprit, ce qu'il dégage, ce qu'il nous communique, avec ses mots d'ordre tacites. La première façon est enclavée par le discours et les rhéteurs qui le possèdent depuis longtemps. L'antagonisme se forme aux limites des possesseurs du discours. Nous devons faire une critique essentielle et radicale du capitalisme et de l'organisation sociale qui en découle. Pour cela, nous devons redevenir détentrices et détenteurs du discours. Nous devons non pas imposer de nouveaux mots d'ordre, tout juste bons à venir mourir dans le vide. La stratégie à adopter est celle de l'ennemi, bien plus maligne, bien plus insidieuse. Il faut repenser toutes nos interactions au monde au travers d'une nouvelle manière d'être, d'une nouvelle matrice : d'une nouvelle idéologie. De là découleront nos nouvelles pratiques. La praxis n'est que l'idéologie en action, et par l'action nous transformerons le monde.

Le capitalisme aura produit le fascisme, l'esclavagisme, le racisme et le patriarcat. Sa matrice idéologique est connue et reconnue : division, compétition quantification, classification. Voilà les quatre cavaliers de l'Apocalypse moderne. Détruire le capitalisme, c'est remplacer cette matrice. Il sera, de facto, inadapté. Pourquoi ? Parce que la nouvelle matrice, cet ensemble d'idées nouvelles, sera non pas anticapitaliste, mais bel et bien non capitaliste. Tout ce qui en découlera ne pourra plus être capitaliste par essence et le spectacle politique s'autodétruira dans l'indifférence générale. Ici, être dangereux, c'est faire vivre et diffuser cette critique radicale du capitalisme, et non pas seulement en faisant péniblement vaciller un des bords du cadre.

Comment créer une nouvelle idéologie et les concepts qui la composent ? N'est-ce pas là la sacro-sainte tâche des philosophes ? Souvenons-nous : la démocratie n'a pas été découverte sous un microscope. Nous aurons donc besoin de tout le monde, car personne n'est étranger à la praxis. Et puisque nous sommes des enfants terribles, nous n'avons qu'à subvertir l'héritage de nos pairs. C'est à elles et eux, et donc à nous, de produire et de rendre vivante cette nouvelle matrice. C'est dans nos nouvelles narrations qu'elle existe, et nous en sommes les cartographes, il nous incombe de montrer les nouvelles voies du possible et du faisable, et d'en faire le chemin du réel.

## La théorie du coup de surin

Avouons-le, le vandalisme n'a pas vraiment la cote. Il est même presque drôle de voir l'incompréhension, le dégoût des autres devant un graffiti. C'est comme si ça provoquait en eux une réaction épidermique, reptilienne. Ce réflexe est notre ennemi, il vise à la conservation de l'esprit, l'imaginaire de celui ou de celle qui regarde : cachez-moi ce tag que je ne saurais voir. Nous voulons tout l'inverse, nos messages sont des invitations vers de nouvelles images, de nouvelles pensées, comme autant de tentatives d'assauts mentaux et spatiaux. Nous devons être plus rapides, plus agiles que ce réflexe. Comment ? Comment jouer les passe-murailles ? En donnant des coups de surin sur les murs, sur les tables, dans les imaginaires. Passer par des voies déjà tracées, pour, au dernier moment, bifurquer, saborder l'esprit. Quelles sont ces "voies déjà tracées" dans les esprits de ceux ou de celles qui nous lirons ? C'est la pop culture. La pop culture crée des imaginaires, peuple les esprits d'histoires, de personnages et d'événements imaginaires, mais aussi d'idées : elle n'est pas et ne sera jamais neutre : c'est la doxa qu'elle distille. En récompensant les bons, en punissant les méchants. Elle consacre ses héros, qui si d'aventure finissaient par aller à son encontre, deviendraient des antihéros, pseudo-cools, pseudo-subversifs, *pseudo-branchés*. Voilà les raccourcis que nous devons emprunter pour parvenir à nos fins : utiliser ses histoires, ses noms et ses formes, les disposer à notre guise pour nos propres luttes. La théorie du coup de surin, c'est la véritable subversion de la pop culture à des fins politiques et idéologiques, subversion nécessaire pour dépasser ce réflexe intégré de protection.

Autre avantage, dans ce combat culturel, elle rend la lutte plus accessible, plus attrayante. Imaginez un "mort aux porcs" accompagné de trois points disposés en triangle : ce code est déjà un truc d'initié, de *connaisseur* : qui sont ces porcs ? Pourquoi vouloir leur mort ? Et ces points ? Que signifient-ils ? À vrai dire, ce message s'adresse directement auxdits porcs qui s'y reconnaîtront. C'est un marqueur territorial plus qu'un appel à la révolte. Prenons maintenant un "Mes potos à moi, ils aiment pas l'État", accompagné d'un A cerclé. Nombreux seront ceux qui ont entendu *Carré d'As* de Jul, et nombreux auront retenu le refrain, et le A, lui aussi, demeurera reconnaissable. "Si les anarchistes aiment Jul et que moi aussi, si ça se trouve, j'suis anarchiste", c'est exactement ce que la théorie du coup de surin cherche à provoquer, de l'association par connivence culturelle, par rapprochement, en rendant plus facile à parcourir la distance entre le spectateur et la lutte, car c'est là qu'il faut les emmener, dans les luttes, les réunions, dans les caves ou à l'arrière des bars, dans les squats, là où nous nous cachons, car ces endroits sont dangereux, des lieux d'instabilité où les cartes sociales peuvent être rebattues : le coup de surin n'est qu'une invitation à cette table.

## Le V-Effekt

Faisons sortir le V-Effekt des théâtres où d'ailleurs plus personne ne va : nous jouons des rôles partout, tout le temps. Tout est performé. Être une femme : performance. Être un homme : performance. Être hétéro : performance. Être homo : performance. Être trans : performance. Être arabe : performance. Être noir·e : performance. Être salarié·e : performance. Être membre de la *start-up nation* : performance. Autant de rôles que de masques, que d'identités, d'autant plus qu'elles se cumulent et s'entrecroisent. Et autant de mauvaises comédies. Nous sommes tous des comédiens-spectateurs et, dans les deux cas, nous ne sommes pas nous. Se distancier et distancier les autres deviennent des nécessités. Nous devons nous distancier de nos non-nous-mêmes.

De quoi se distancier et surtout, de quoi se rapprocher ? Il faut faire la distinction entre "nos rôles" et "notre rôle". Le premier est un ensemble de masques iniques, l'autre est notre personne prise sans segment, dans sa complexité assumée, pleine et entière. Ce que l'*on* veut que vous soyez, contre ce que vous êtes (ou voudriez être). Nos rôles ne sont que des formes d'aliénation, d'adaptation, d'interfaces entre notre rôle et ce monde absurde. Ce que l'*on* nous fait faire contre ce que l'*on* joue, ou plutôt ce que l'*on* ne joue pas. Notre part subie, contre notre part choisie. Être dangereux, c'est être en mesure de choisir.

Pour se distancier, nous devons nous distancier de nos histoires anciennes, de nos mythes rances. *Tout est à refaire*. Arrêtons de performer, commençons à vivre. Nos vies ne sont ni des italiennes ni des allemandes : *fais-le ou ne le fais pas, mais il n'y a pas d'essai*.

Le V-Effekt doit alors être partout. Les spectateurs, en nous voyant, ne doivent plus être saisis de terreur, mais tressaillir devant cette liberté d'un seul coup accessible, à portée de main et d'esprit. De ce petit *Organon*, souvenons-nous de la 38^e^ entrée : "Les conditions historiques \[et sociales\] ne doivent pas être conçues comme des puissances obscures, elles sont au contraire créées et maintenues par les hommes (et seront changées par eux)." D'où la nécessité de repolitiser, de *réidologiser* le réel. Sans ça, notre représentation serait ratée. Nous devons rendre aux spectateurs permanents, et donc à nous aussi, la capacité de lire le drame social qui se joue avec eux, mais surtout, d'eux.

## La poésie

Dans les écrits jakobsoniens, la poésie a une fonction particulière. C'est la fonction sans fonction, la fonction qui est, et dont c'est le seul mérite. Cette fonction ne sert à rien dans le modèle communicatif. Elle est fondamentale dans son inutilité. En n'étant rien d'autre qu'elle-même et pourtant indispensable, elle nous apprend deux choses. La première est que l'inutilité est indispensable, la seconde est que la poésie est au centre de tout.

Repensons alors notre définition de la poésie. La poésie, c'est tout ce qui ne sert à rien. L'inutilité est poésie, la poésie est inutilité. Tous les vers du monde n'épuisent pas ce concept de poésie. Notre nouvelle poésie est enrichie, son concept élargi à tout ce qui n'a pas de fonction, pas de valeur. Ainsi l'humain devient poésie : indispensable, sans fonction ni destination.

Maintenant, reprenons cette notion élargie dans la *superstructure* capitaliste, cette idéologie, cette ode à la productivité, à la quantification et à la division. La poésie en devient la némésis totale et complète. La poésie ne peut pas devenir marchandise, elle est invendable, car sa valeur d'usage est infinie (puisqu'elle est indispensable) et sa valeur d'échange est nulle puisqu'elle ne sert à rien. Sa valeur n'est donc ni nulle ni inexistante : elle est impossible à déterminer. La poésie est un anticoncept capitaliste. La poésie étendue devient, par son essence, une anti-idée de la *superstructure* capitaliste, et grâce à la réintégration de l'humain dans le royaume de la poésie, nous pouvons dépasser la détermination du prolétariat.

Ainsi la poésie devient dangereuse.

## Ce qu'il faut combattre

S'il faut être dangereux, il faut aussi combattre tout ce qui nous étouffe. Tout ce qui empêche la réinterprétation, la reconsidération, le pas de côté est ennemi. Le premier est donc l'idiot : à la fois comme concept et comme personnage. C'est l'incapacité consentie ou non de ne pas comprendre, de ne pas voir plus loin que soi-même, c'est ça être idiot. C'est le refus de la notion même de société, d'ensemble, de corps et du jeu de son organisation : la politique. L'idiot produit la bêtise crasse, marque du laid. Cette mélasse noire, appesantissante, qui rapporte tout à lui, à sa personne. Nous n'avons besoin ni de cette crasse ni des individus qui la produisent. Les agents de la doxa, les apôtres de la *superstructure*, ces technoprêtres, sont nos ennemis. Eux pour leurs actes, leurs discours, leurs rôles, mais en aucun cas pour leur personne. Peut-être est-ce une erreur de les tolérer ainsi, eux et la crasse qu'ils produisent, mais frapper sur la gueule d'un con ne l'a jamais rendu intelligent (même si c'est vrai que ça fait du bien).

Au laid, à l'idiot et à la doxa (les idées de la bourgeoisie et le discours autour de ses idées), que pouvons-nous ajouter ? L'ennui ? Pas tout à fait. Disons plutôt la routine. L'habitude, cette rouille, cette forme de non-vie, vendue comme pinacle de la bonne vie, de la bonne manière. Celle, qui, justement, dépolitise le plus le réel. La routine, par sa capacité à dépolitiser ou plutôt à désidéologiser (faussement), est pernicieuse. La normalité n'existe pas. Rien n'est normal, la normalité n'a aucune réalité sociale. La normalité, c'est ce à quoi nous nous sommes habitués, ce que nous tolérons dans ce fascisme ordinaire. Normalité, ce camouflage de tout ce que nous détestons, n'est que la norme qu'*on* s'inflige, dont *on* se mutile. Voilà un ennemi.

## Que penser de la politique ?

La politique est un produit de l'idéologie : des manières de voir, de percevoir tout ce qui n'est pas moi, l'extérieur, le monde, les autres, font naître la manière dont je veux organiser le monde social auquel j'appartiens et épuise cette naissance.

L'idéologie dominante est celle de la division. Nous divisons tout et tout nous divise. Nos vies sont compartimentées, aussi bien dans le temps que dans l'espace. Un jour, nous allons à l'école, un jour nous sommes employés puis, enfin, nous devenons des retraités, c'est bien une division temporelle de nos vies, régies, édictées par des décisions politiques. Diviser, organiser : voilà bien la mission de la chose politique. Il y a le légal et l'illégal. Nous ? Nous sommes ailleurs.

Même les classes politiques sont divisées : il y a la gauche et la droite, mais la gauche et la droite de quel objet ? Ce qui divise ces quelques pourceaux, c'est la question de la répartition des richesses. La question est bien "Que faire de la masse des richesses produites ?" C'est tellement évident que toutes les questions qui ne sont pas d'ordre économique deviennent des questions "sociétales"[^1-instantanes_1]. Le problème étant que cette richesse est divisible et produite par une foule hétéroclite de gens tous différents[^1-instantanes_2] les uns des autres, mais constitués en une masse indifférenciée (l'horreur, presque la définition du vivant, imaginez un peu). Des gens s'écharpent jour et nuit pour répondre à cette question idiote. Certains disent que la richesse doit revenir aux membres de cette masse, dans un esprit d'égalité et de proportionnalité. D'autres, d'encore plus idiots, disent que les richesses doivent retourner à ceux qui l'ont déjà, comme un juste retour de leur participation, de leur inaction... existentielle : bref, parce que. Toute autre personne choisissant une autre réponse passera pour un imbécile, un bouffon, un enfant à cajoler, mais devra laisser les adultes réfléchir entre eux, parce que, *en même temps*, ces choses-là sont sérieuses. Voilà le spectre de la politique, l'horizon. Mais cette politique est le produit d'une idéologie. Dès lors, toutes celles et ceux que se projettent dans cette fausse division rentrent dans ce jeu politique, dans ce monde rance. Et avec ces gens-là, nous n'avons rien en commun, notre combat n'est pas et ne sera jamais politique, il est idéologique. C'est le point d'origine de notre désertion sociale, de notre radicalité, de notre refus de la hiérarchie, mais aussi du salariat, de toute forme de division et de tous ses produits. Ainsi, tout ce qui sera issu de notre idéologie réunificatrice, collégiale, collective n'aura rien en commun avec cette mascarade. Ainsi, l'avenir sera *beau* : la volupté, c'est le retour du *beau* en politique.

```{=tex}
\cleardoublepage
\begin{nsright}
\itshape
\thispagestyle{empty}
\vspace*{9\baselineskip}

Y a-t-il un monde après l'oubli ?\\
« Ravage, Ravage, Ravage »,\\
Étaient-ce les oiseaux qui scandaient son nom ?

\end{nsright}
```
# Les mots n'épuisent pas les concepts

```{=tex}
\epigraphe{L'ordre naturel, celui de la fraternité entre les hommes.}{Luis Sepúlveda}{Le neveu d'Amérique}
```
*On* peut juger cette phrase maladroite dans sa forme, elle reste vraie dans ce qu'elle tente de définir. Cependant, elle n'épuise pas le concept qu'elle tente de décrire : les mots n'épuisent pas les concepts.

## Un mot neuf est une poésie

Nous pourrions mettre n'importe quel mot dessus : solidarité, fraternité, adelphité, respect dû entre membres d'une même espèce, humanisme. Nous pourrions définir ces mots des heures durant pour les faire coller le plus possible à ce concept, pour définir "l'ordre naturel", l'ordre humain (l'ordre sans le pouvoir ?) : l'anthropie. Un mot nouveau, libre, pour un concept ancien et pollué par la matérialité du langage. Nous ne voulons pas empêcher la vie, au contraire, nous la souhaitons simplement lavée du pouvoir ; plus précisément encore, nous ne refusons pas l'idée de pouvoir (refuser une idée est absurde), nous refusons sa réification dans le corps social sous ses formes et ses instances les plus perverses : la division et la domination. Ces deux choses-là ont une matérialité et peuvent, dès lors, être combattues et refusées. Alors l'anthropie, ce concept, ce tribut à notre humanité sera toujours mal écrit, mal décrit, mal formulé, mal formé par des mots. Parce que les mots n'épuisent pas les concepts.

## Définir le concept non pas par d'autres mots, mais par l'expérience qu'il produit et les sensations qu'il peut procurer

Essayer le plus possible de s'en approcher par les mots est une activité, c'est faire de la philosophie, *connaissance des principes premiers et connaissance des causes ultimes*. Les idées, les concepts, parce qu'inépuisables, sont des principes premiers. La science, l'étude des discours autour de ces idées et de leurs réalisations, dans et par le social, c'est l'idéologie. L'idéologie est une philosophie appliquée. La politique n'est que l'ordonnancement et la réalisation dans et par le social de ces idées. Une des tâches de l'in-situ-arsonisme est de réduire la distance entre l'idée d'anthropie et son ressenti. Nous devons trouver des moyens de la faire ressentir. La définir serait une insulte, nous ne sommes pas philosophes, et toute forme matérialisée par des mots serait imparfaite.

Comment définir le ressenti de l'anthropie, qui, de par sa matérialité, peut-être discutée, voire définie, qui sait ? Essayons ! Ça serait l'expérience de la chaleur humaine par la mise sous tension (les situations dangereuses) de l'énergie humaine, ce qui rend l'expérience, le moment, nécessairement collégial, provoqué et puisque vécu à plusieurs, poétique puisque invendable, éphémère et impalpable, c'est-à-dire *a-capitaliste*. Reste à savoir comment le rendre politique. À quoi bon vouloir le rendre politique ? L'in-situ-arsonisme n'est politique que par écho, que par *réseau-nance*. Il n'est que le lieu, *la situation*, où viennent se confondre philosophie, idéologie et politique.

Pourquoi refuser le politique ? Parce qu'en l'état, l'Utopie est irréalisable. *On* ne peut pas, aujourd'hui, la rendre matérielle. Pourquoi ? Parce que l'existence de cette idée est trop peu répandue. L'Utopie est un projet collectif d'existence (qui ne nierait pas l'existence individuelle). Il faut donc rappeler d'abord les idées qui la composent, idées pulvérisées tous les jours par l'idéologie dominante, le capitalisme et sa matrice. Il faut embrasser l'interdépendance, pour ne pas dire la symbiose, entre l'idée et sa réalisation : déployer les idées nécessaires à l'Utopie tout en la réalisant, c'est-à-dire la vivre et la faire vivre dans des espaces (réels ou imaginaires, fixes ou mobiles). Ici et maintenant, oui, mais si tout niquer seul, j'en ai jamais rêvé, nous aurons toujours mal en voyant nos semblables, en bas, faire *rampampampamp*.

## L'espace et ses divisions

Reste une question en suspens : Comment appeler à la fin des fausses divisions et oser séparer la philosophie de l'idéologie et de la politique ? Ne serait-ce pas là aussi une fausse division ? Nous répondrons alors que c'est une simple adaptation aux institutions elles-mêmes divisées et incapables de recevoir ce discours. C'est une tentative de placement, dans cet espace divisé. Arbitrairement divisé, mais dont les frontières sont pleinement réifiées dans notre système éducatif, universitaire et intellectuel. Ce placement est, de par son positionnement dans cet espace fractionné, impossible à clairement expliciter. Serait-ce alors un concept ? L'in-situ-arsonisme existe dans sa pratique, c'est une praxis, et comme toutes les praxis, elle se doit de transformer le monde, d'avoir sa propre matérialité. Il est cependant, dans son essence, polymorphe. La polymorphie qui appelle à sa propre unification, l'appel de l'Utopie à elle-même.
[^1-instantanes_1]: Amis, adelphes, frères et sœurs, croire que vos identités intéressent ces gens-là est un mensonge que vous vous racontez entre vous. Ils n'en ont rien à foutre de vos droits, ils veulent juste vos thunes\.

[^1-instantanes_2]: Et c'est bien d'ailleurs pour cela que dans le cadre de cette politique, vous serez simplement tolérés et jamais acceptés, *on* vous demande d'être rentables et de rentrer dans le moule économique. Du reste, ils s'en foutent\.
