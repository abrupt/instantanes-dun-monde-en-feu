# ~/ABRÜPT/MAGUS/INSTANTANES-DUN-MONDE-EN-FEU/*

La [page de ce livre](https://abrupt.cc/magus/instantanes-dun-monde-en-feu/) sur le réseau.

## Sur l'ouvrage

Si les institutions sont des morceaux de sociohistoire cristallisés alors ce manuel se souhaite comme un creuset. L'in-situ arsonisme n'est qu'un prétexte, une invitation à la démystification. L'aura du politique est une farce. Les postures deviennent alors des cadavres. Nous dansons en écoutant la saudade du monde qui brûle. Mort à ceux qui nous divisent. Revendiquons notre nous. Notre nous contre leur nous. Eux ne s'en cachent pas. Alors dans les coteries, les dernières et les derniers s'organisent. Nouvelle praxis des corps en devenir, irruption des idées dans le réel. Partout les choses se reforment. Ici quelques prémices, les tisons sont encore chauds. Les pages bullent doucement d'un éther pourpre.

L'in-situ arsonisme est la recherche d'une nouvelle mystique et cette mystique, légère, dévorante, lancinante, se pose une question : que faire de la misère de nos milieux ? Comment être dangereux ? Comment gagner la guerre ? Les digressions dans le liquide syntagmatique sont brumeuses, les alambics sont trop nombreux. Que faire pour agir ? Foutre le feu, partout. D'abord dans nos esprits inondés d'informations. Il faudra bien apprendre à résister.

Et à toutes celles, et à tous ceux qui pensent pouvoir faire ça seul·e·s, un avertissement : votre bêtise ne sera pas permise ici. La révolution s'organise. Nous ne sommes pas seul·e·s. Aussi une mission se dessine : celle de le rappeler.

## Sur l'auteur

Connard lambda.

## Sur la licence

Cet [antilivre](https://abrupt.cc/antilivre/) est disponible sous licence Creative Commons Attribution – Pas d’Utilisation Commerciale – Partage dans les Mêmes Conditions 4.0 International ([CC-BY-NC-SA 4.0](LICENSE-TXT)).

La version HTML de cet antilivre est placée sous [licence MIT](LICENSE-MIT).

## Etc.

Vous pouvez également découvrir notre [site](https://abrupt.cc) pour davantage d'informations sur notre démarche, notamment quant au [partage de nos textes](https://abrupt.cc/partage/).
